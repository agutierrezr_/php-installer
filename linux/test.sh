#!/bin/bash

composer --version

phpcs --version
phpcbf --version

php5.6
php --version | grep "PHP 5.6"
phpunit --version | grep "PHPUnit 5"
phpcs
phpunit -c phpunit5.xml

php7.0
php --version | grep "PHP 7.0"
phpunit --version | grep "PHPUnit 6"
phpcs
phpunit -c phpunit6.xml

php7.1
php --version | grep "PHP 7.1"
phpunit --version | grep "PHPUnit 7"
phpcs
phpunit -c phpunit7.xml

php7.2
php --version | grep "PHP 7.2"
phpunit --version | grep "PHPUnit 7"
phpcs
phpunit -c phpunit7.xml
