#!/bin/bash

if [[ $EUID -ne 0 ]]; then
  echo "This script must be run as root, use sudo "$0" instead" 1>&2
  exit 1
fi

export DEBIAN_FRONTEND="noninteractive"

echo 'Update packages'
apt-get update > /dev/null

echo 'Install required linux packages'
apt-get install apt-transport-https apt-utils ca-certificates git lsb-release software-properties-common unzip wget -y > /dev/null

echo 'Download gpg certificate for sury'
wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg > /dev/null

echo 'Add Sury repository'
sh -c 'echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" > /etc/apt/sources.list.d/php.list'

echo 'Update packages'
apt-get update > /dev/null
