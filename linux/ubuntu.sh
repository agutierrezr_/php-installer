#!/bin/bash

if [[ $EUID -ne 0 ]]; then
  echo "This script must be run as root, use sudo "$0" instead" 1>&2
  exit 1
fi

export DEBIAN_FRONTEND="noninteractive"

echo 'Update packages'
apt-get update > /dev/null

echo 'Install required linux packages'
apt-get install apt-utils git lsb-release software-properties-common unzip wget -y > /dev/null

echo 'Add ondrej repository for PHP'
touch /etc/apt/sources.list.d/php-$(lsb_release -sc).list
echo "deb http://ppa.launchpad.net/ondrej/php/ubuntu $(lsb_release -sc) main" >> /etc/apt/sources.list.d/php-$(lsb_release -sc).list
echo "deb-src http://ppa.launchpad.net/ondrej/php/ubuntu $(lsb_release -sc) main" >> /etc/apt/sources.list.d/php-$(lsb_release -sc).list
apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E5267A6C

echo 'Update packages'
apt-get update > /dev/null
