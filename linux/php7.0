#!/bin/bash

if [[ $EUID -ne 0 ]]; then
echo "This script must be run as root, use sudo "$0" instead" 1>&2 ; exit 1 ;
fi

echo "Remove symbolic links for current PHP version"
rm -r /etc/alternatives/phar* > /dev/null
rm -r /etc/alternatives/php* > /dev/null

echo "Create symbolic links for PHP 7.0"
ln -s /usr/bin/phar7.0 /etc/alternatives/phar > /dev/null
ln -s /usr/share/man/man1/phar7.0.1.gz /etc/alternatives/phar.1.gz > /dev/null
ln -s /usr/bin/phar.phar7.0 /etc/alternatives/phar.phar > /dev/null
ln -s /usr/share/man/man1/phar.phar7.0.1.gz /etc/alternatives/phar.phar.1.gz > /dev/null
ln -s /usr/bin/php7.0 /etc/alternatives/php > /dev/null
ln -s /usr/share/man/man1/php7.0.1.gz /etc/alternatives/php.1.gz > /dev/null
ln -s /usr/bin/php-config7.0 /etc/alternatives/php-config > /dev/null
ln -s /usr/share/man/man1/php-config7.0.1.gz /etc/alternatives/php-config.1.gz > /dev/null
ln -s /usr/bin/phpsize7.0 /etc/alternatives/phpsize > /dev/null
ln -s /usr/share/man/man1/phpsize7.0.1.gz /etc/alternatives/phpsize.1.gz > /dev/null

echo "Remove current phpunit version"
rm /usr/local/bin/phpunit > /dev/null

echo "Copy phpunit binary for PHP7.0"
cp /usr/local/bin/phpunit-6 /usr/local/bin/phpunit > /dev/null

echo "Disable current PHP version in apache"
a2dismod php5.6 > /dev/null
a2dismod php7.1 > /dev/null
a2dismod php7.2 > /dev/null

echo "Enable PHP 7.0 in apache"
a2enmod php7.0 > /dev/null

echo "Restart apache"
service apache2 restart > /dev/null
