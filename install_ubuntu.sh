#!/bin/bash

apt install git -y
git clone https://gitlab.com/franicflowsarl/php-installer.git
chmod +x php-installer/linux/ubuntu.sh php-installer/linux/install.sh
cd php-installer
./linux/ubuntu.sh
./linux/install.sh
cd ..
rm -rf php-installer
