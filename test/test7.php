<?php

/**
 * Unit tests for PHPUnit 7
 *
 * PHP Version 7.1, 7.2
 *
 * @category  Test
 * @package   PHPInstaller
 * @author    Nhu-Hoai Robert VO <nhuhoai.vo@franicflow.ch>
 * @copyright 2018 FRANIC Flow Sàrl
 * @license   No license
 * @version   GIT: 2.1.0
 * @link      https://gitlab.com/franicflowsarl/php-installer
 * @since     2.1.0
 */

 declare(strict_types=1);

 use PHPUnit\Framework\TestCase;

/**
 * Unit tests for PHPUnit 7
 *
 * PHP Version 7.1, 7.2
 *
 * @category  Test
 * @package   PHPInstaller
 * @author    Nhu-Hoai Robert VO <nhuhoai.vo@franicflow.ch>
 * @copyright 2018 FRANIC Flow Sàrl
 * @license   No license
 * @link      https://gitlab.com/franicflowsarl/php-installer
 * @since     0.1.0
 */
final class Test extends TestCase
{
    /**
     * 1 = 1
     *
     * @return void
     */
    public function test1Equals1()
    {
        $a = 1;
        $this->assertEquals($a, 1);
    }

    /**
     * 2 greater than 1
     *
     * @return void
     */
    public function test2great1()
    {
        $this->assertTrue(2 > 1);
    }

    /**
     * String not equals
     *
     * @return void
     */
    public function testTextNotEquals()
    {
        $this->assertFalse("A" == "B");
    }
}

?>
