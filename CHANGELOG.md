# Changelog

[t/0.2.1]: https://gitlab.com/franicflowsarl/php-installer/tags/0.2.1
[v/0.2.1]: https://img.shields.io/badge/version-0.2.1-blue.svg
[b/0.2.1]: https://gitlab.com/franicflowsarl/php-installer/badges/0.2.1/pipeline.svg

[![][v/0.2.1]][t/0.2.1]
[![][b/0.2.1]][t/0.2.1]

#### Fixes

-   Missing dependencies for PHPCS (add php xml package)

#### Additions

-   One line script install
-   Test phpcs and phpunit commands with fake data
-   Custom home page

<hr />

[t/0.2.0]: https://gitlab.com/franicflowsarl/php-installer/tags/0.2.0
[v/0.2.0]: https://img.shields.io/badge/version-0.2.0-blue.svg
[b/0.2.0]: https://gitlab.com/franicflowsarl/php-installer/badges/0.2.0/pipeline.svg

[![][v/0.2.0]][t/0.2.0]
[![][b/0.2.0]][t/0.2.0]

#### Additions

-   PHP_CodeSniffer

<hr />

[t/0.1.0]: https://gitlab.com/franicflowsarl/php-installer/tags/0.1.0
[v/0.1.0]: https://img.shields.io/badge/version-0.1.0-blue.svg
[b/0.1.0]: https://gitlab.com/franicflowsarl/php-installer/badges/0.1.0/pipeline.svg

[![][v/0.1.0]][t/0.1.0]
[![][b/0.1.0]][t/0.1.0]

**Initial release**

#### Additions

-   PHP (versions 5.6, 7.0, 7.1, 7.2)
-   Composer
-   PHPUnit
